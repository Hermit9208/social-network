import profileReducer from "./profileReducer";
import dialogsReducer from "./dialogsReducer";
import sidebarReducer from "./sidebarReducer";

const store = {
   _state: {
      profilePage: {
         posts: [
            {name: 'Dima', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'},
            {name: 'Aidina', text: 'Aliquid atque consectetur dolorem dolorum eos ex laboriosam magni'},
            {name: 'Talgat', text: 'Minus nam nostrum perferendis quae quia quidem quod reprehenderit repudiandae soluta temporibus veniam.'}
         ],
         newPostText: 'it-kamasutra'
      },
      dialogsPage: {
         dialogsData: [
            {id: 0, name: 'Dima'},
            {id: 1, name: 'Kesha'},
            {id: 2, name: 'Eleman'},
            {id: 3, name: 'Kostya'},
            {id: 4, name: 'John'},
            {id: 5, name: 'Nasty'},
         ],
         messageData: [
            {id: 0, text: 'Hi!'},
            {id: 1, text: 'Hello!'},
            {id: 2, text: 'How are you?'}
         ],
         messageNewBody: ''
      },
      navBarLink: ['profile', 'dialogs', 'news', 'music', 'settings']
   },
   _callSubscriber() {
      console.log('State changed')
   },

   getState() {
     return this._state
   },
   subscribe(observer) {
      this._callSubscriber = observer
   },

   dispatch(action) { // {type: "..."}
      this._state.profilePage = profileReducer(this._state.profilePage, action);
      this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
      this._state.navBarLink = sidebarReducer(this._state.navBarLink, action);
      this._callSubscriber(this._state)
   }
};





export default store;