const UPDATE_NEW_MESSAGE_BODY = 'UPDATE_NEW_MESSAGE_BODY';
const SEND_MESSAGE = 'SEND_MESSAGE';

let initialState = {
   dialogsData: [
      {id: 0, name: 'Dima'},
      {id: 1, name: 'Kesha'},
      {id: 2, name: 'Eleman'},
      {id: 3, name: 'Kostya'},
      {id: 4, name: 'John'},
      {id: 5, name: 'Nasty'},
   ],
   messageData: [
      {id: 0, text: 'Hi!'},
      {id: 1, text: 'Hello!'},
      {id: 2, text: 'How are you?'}
   ],
   messageNewBody: ''
};

const dialogsReducer = (state = initialState, action) => {
   switch (action.type) {
      case UPDATE_NEW_MESSAGE_BODY:
         return  {...state, messageNewBody: action.body};
      case SEND_MESSAGE:
         return  {
            ...state,
            messageData: [...state.messageData, {id: 3, text: action.message}],
            messageNewBody: ''
         };
      default:
         return state;
   }
};

export const sendMessageCreator = (message) => ({type: SEND_MESSAGE, message});
// export const updateNewMessageBodyCreator = (body) => ({
//    type: UPDATE_NEW_MESSAGE_BODY,
//    body: body
// });

export default dialogsReducer;