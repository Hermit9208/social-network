import {profileAPI} from "../api/api";

const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE_NEW_POST_TEXT';
const SET_USER_PROFILE = 'SET_USERS_PROFILE';
const SET_STATUS = 'SET_STATUS';

let initialState = {
   posts: [
      {name: 'Dima', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'},
      {name: 'Aidina', text: 'Aliquid atque consectetur dolorem dolorum eos ex laboriosam magni'},
      {name: 'Talgat', text: 'Minus nam nostrum perferendis quae quia quidem quod reprehenderit repudiandae soluta temporibus veniam.'}
   ],
   newPostText: 'it-kamasutra',
   profile: null,
   status: ''
};

const profileReducer = (state = initialState, action) => {
   switch (action.type) {
      case ADD_POST: {
         const newPost = {
            name: 'Talgat',
            text: action.newText
         };
         return  {
            ...state,
            posts: [...state.posts, newPost]
         };
      }
      // case UPDATE_NEW_POST_TEXT: {
      //    return {
      //       ...state,
      //       newPostText: action.newText
      //    };
      // }
      case SET_USER_PROFILE: {
         return {
            ...state,
            profile: action.profile
         };
      }
      case SET_STATUS: {
         return {
            ...state,
            status: action.status
         }
      }
      default:
         return state
   }
};

export const addPostActionCreator = (value) => ({type: ADD_POST, newText: value});
export const updateNewPostTextActionCreator = (text) => ({
   type: UPDATE_NEW_POST_TEXT,
   newText: text
});
export const setUsersProfile = (profile) => ({
   type: SET_USER_PROFILE,
   profile
});
export const setStatus = (status) => ({type: SET_STATUS, status});


export const getUserProfile = (userId) => {
   return (dispatch) => {
      profileAPI.getProfile(userId)
         .then(data => {
            dispatch(setUsersProfile(data));
         });
   }
};

export const getStatus = (userId) => {
   return (dispatch) => {
      profileAPI.getStatus(userId)
         .then(response => {
            dispatch(setStatus(response.data))
         })
   }
};
export const updateStatus = (status) => {
   return (dispatch) => {
      profileAPI.updateStatus(status)
         .then(response => {
            if (response.data.resultCode === 0) {
               dispatch(setStatus(status))
            }
         })
   }
};

export default profileReducer;