import axios from "axios";

const instance = axios.create({
   withCredentials: true,
   baseURL: 'https://social-network.samuraijs.com/api/1.0/',
   headers: {
      "API-Key": 'b3dc7446-b549-4e87-8731-5023d5d3d261'
   }
});

export const usersAPI = {
   getUsers(currentPage = 1, pageSize = 10) {
      return instance.get(`users?page=${currentPage}&count=${pageSize}`)
         .then(response => {
            return response.data
         })
   }
};

export const followAPI = {
   followUser(id) {
      return instance.post(`follow/${id}`)
         .then(response => {
            return response.data
         })
   },
   unfollowUser(id) {
      return instance.delete(`follow/${id}`)
         .then(response => {
            return response.data
         })
   }
};

export const authAPI = {
   authUser() {
      return instance.get(`auth/me`)
         .then(response => {
            return response.data
         })
   },
   loginUser (email, password, rememberMe = false) {
      return instance.post(`auth/login`, {email, password, rememberMe})
   },
   logOutUser () {
      return instance.delete(`auth/login`)
   }
};

export const profileAPI = {
   getProfile(userId) {
      return instance.get(`profile/${userId}`)
         .then(response => {
            return response.data
         })
   },
   getStatus (userId) {
      return instance.get(`profile/status/${userId}`)
   },
   updateStatus (status) {
      return instance.put(`profile/status`, {status: status})
   }
};


