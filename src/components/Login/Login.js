import React from 'react';
import s from './Login.module.css'
import {Field, reduxForm} from "redux-form";
import {Input} from "../Comon/FormsControl/FormsControl";
import {required} from "../../utils/validator";
import {connect} from "react-redux";
import {login} from "../../redux/authReducer";
import {Redirect} from "react-router-dom";

const LoginForm = (props) => {
   return (
      <form onSubmit={props.handleSubmit} className={s.form}>
         <label htmlFor="">
            <Field
               placeholder={'Email'}
               name={'email'}
               component={Input}
               validate={[required]}
            />
         </label>
         <label htmlFor="">
            <Field
               placeholder={'Password'}
               name={'password'}
               type={'password'}
               component={Input}
               validate={[required]}
            />
         </label>
         <label htmlFor="">
            <Field
               component={Input}
               name={'rememberMe'}
               type={'checkbox'}
            /> remember me
         </label>
         {props.error && <p className={s.formSummaryError}>{props.error}</p>}
         <button>Login</button>
      </form>
   );
};

const LoginReduxForm = reduxForm({
   form: 'login'
})(LoginForm);


const Login = (props) => {
   const onSubmit = (formData) => {
      props.login(formData.email, formData.password, formData.rememberMe)
   };

   if (props.isAuth) return <Redirect to={'/profile'}/>;

   return (
      <>
         <h1 className={s.title}>Login</h1>
         <LoginReduxForm onSubmit={onSubmit}/>
      </>
   );
};

const mapStateToProps = (state) => ({
   isAuth: state.auth.isAuth
});

export default connect(mapStateToProps, {login})(Login);