import React from 'react';
import cn from './Users.module.css'
import userAvatar from '../../assets/images/user-avatar.png'
import {NavLink} from "react-router-dom";

const Users = (props) => {
   let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
   let pages = [];
   for (let i = 1; i <= pagesCount; i++) {
      pages.push(i);
   }

   return (
      <div>
         <h1 className={cn.UserTitle}>Users</h1>
         {props.users.map(user => (
            <div className={cn.User} key={user.id}>
               <div className={cn.UserLeft}>
                  <NavLink to={'profile/' + user.id}>
                     <img src={user.photos.small !== null ?
                        user.photos.small :
                        userAvatar
                     } alt=""/>
                  </NavLink>
                  {user.followed
                     ? <button
                        disabled={props.followingInProgress.some(id => id === user.id)}
                        onClick={() => props.unfollow(user.id)}
                     >
                        Unfollow
                     </button>
                     : <button
                        disabled={props.followingInProgress.some(id => id === user.id)}
                        onClick={() => props.follow(user.id)}
                     >
                        Follow
                     </button>
                  }
               </div>
               <div className={cn.UserInfo}>
                  <div className={cn.UserInfoLeft}>
                     <p>{user.name}</p>
                     <p className={cn.UserInfoLeft__Status}>{user.status}</p>
                  </div>
                  <div className={cn.UserInfoRight}>
                     <p>{'user.location.country'}</p>
                     <p>{'user.location.city'}</p>
                  </div>
               </div>
            </div>
         ))}
         <div className={cn.paginationBlock}>
            {pages.map(page => (
               <span onClick={() => props.onPageChanged(page)}
                     className={`${props.currentPage === page && cn.selectedPage} ${cn.pagination}`}>
                     {page}
               </span>
            ))}
         </div>
      </div>
   )
};

export default Users;