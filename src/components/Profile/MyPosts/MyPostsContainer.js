import MyPosts from "./MyPosts";
import {addPostActionCreator, updateNewPostTextActionCreator} from "../../../redux/profileReducer";
import {connect} from "react-redux";

let mapStateToProps = (state) => {
   return {
      posts: state.profilePage.posts,
      newPostText: state.profilePage.newPostText
   }
};

let mapDispatchToProps = (dispatch) => {
   return {
      // updateNewPostText: (value) => {
      //    let action = updateNewPostTextActionCreator(value);
      //    dispatch(action)
      // },
      addPost: (value) => {
         dispatch(addPostActionCreator(value))
      }
   }
};

const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts);

export default MyPostsContainer;