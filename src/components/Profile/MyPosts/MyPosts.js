import React from 'react';
import classes from "./MyPosts.module.css";
import Post from "./Post/Post";
import {Field, reduxForm} from "redux-form";
import {maxLengthCreator, required} from "../../../utils/validator";
import {Textarea} from "../../Comon/FormsControl/FormsControl";

const maxLength10 = maxLengthCreator(10);

const MyPostsForm = (props) => {
   return (
      <form onSubmit={props.handleSubmit}>
         <label>
               <Field className={classes.userPost__text}
                      component={Textarea}
                      name={'post'}
                      placeholder={props.newPostText}
                      validate={[required, maxLength10]}
               />
         </label>
         <button className={classes.btn}>Send</button>
      </form>
   )
};

const MyPostsReduxForm = reduxForm({
   form: 'posts'
})(MyPostsForm);

const MyPosts = (props) => {
   const onSubmit = (formData) => {
      debugger
      console.log(formData);
      props.addPost(formData.post);
   };

   return (
      <>
         <div className={classes.userPost}>
            <h2 className={classes.title}>My posts</h2>
            <MyPostsReduxForm onSubmit={onSubmit} newPostText={props.newPostText}/>
         </div>
         <div className={classes.allMessage}>
            {props.posts.map((post, index) => <Post key={index} post={post}/>)}
         </div>
      </>
   );
};

export default MyPosts;