import React from 'react';
import classes from "./Post.module.css";
import userAvatar from '../../../../assets/images/user-avatar.png'

const Post = ({post}) => {
   const {name, text} = post;
   return (
      <div className={classes.userMessage}>
         <img src={userAvatar} alt="user-small"/>
         <p>
            {name} - {text}
            <a href="/">like</a>
         </p>
      </div>
   );
};

export default Post;