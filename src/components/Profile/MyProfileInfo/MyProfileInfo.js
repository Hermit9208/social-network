import React from 'react';
import classes from "./MyProfileInfo.module.css";
import Preloader from "../../Comon/Preloader/Preloader";
import jsImage from "../../../assets/images/js.jpg"
import ProfileStatus from "./ProfileStatus/ProfileStatus";

const MyProfileInfo = (props) => {
   if (!props.profile) {
      return <Preloader/>
   }
   return (
      <>
         <img className={classes.image} src={jsImage} alt="js"/>
         <div className={classes.userInfo}>
            <img className={classes.userInfo__image} src={props.profile.photos.small} alt={props.profile.fullName}/>
            <div>
               <h1>{props.profile.fullName}</h1>
               <ul>
                  <li>Contacts: {props.profile.contacts.github}</li>
                  <li>City: Minsk</li>
                  <li>Education: BSU 11</li>
                  <li>Web site: https://it-kamasutra.com</li>
               </ul>
            </div>
         </div>
         <ProfileStatus status={props.status} updateStatus={props.updateStatus}/>
      </>
   );
};

export default MyProfileInfo;