import React from 'react';
import classes from './Profile.module.css';
import MyProfileInfo from "./MyProfileInfo/MyProfileInfo";
import MyPostsContainer from "./MyPosts/MyPostsContainer";

const Profile = (props) => {
   return (
      <div className={classes.content}>
         <MyProfileInfo
            profile={props.profile}
            status={props.status}
            updateStatus={props.updateStatus}
         />
         <MyPostsContainer/>
      </div>
   );
};

export default Profile;