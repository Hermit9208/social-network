import React from 'react';
import s from './Dialogs.module.css'
import FriendsList from "./FriendsList/FriendsList";
import Message from "./Message/Message";
// import {Redirect} from "react-router-dom";
import {Field, reduxForm} from "redux-form";
import {Textarea} from "../Comon/FormsControl/FormsControl";
import {maxLengthCreator, required} from "../../utils/validator";

const maxLength50 = maxLengthCreator(50);
const DialogsForm = (props) => {
   return (
      <form onSubmit={props.handleSubmit}>
         <label htmlFor="">
            <Field
               component={Textarea}
               name={'message'}
               placeholder="your massage"
               validate={[required, maxLength50]}
            />
         </label>
         <button>Send</button>
      </form>
   )
};

const DialogsFormRedux = reduxForm({
   form: 'message'
})(DialogsForm);

const Dialogs = (props) => {
   const getState = props.dialogsPage;

   const onSubmit = (dataForm) => {
      console.log(dataForm);
      props.sendMessageClick(dataForm.message);
   };


   // if (!props.isAuth) return <Redirect to={'/login'}/>;

   return (
      <div className={s.dialogs}>
         <h1 className={s.friends__title}>Dialogs</h1>
         <ul className={s.friendsList}>
            {getState.dialogsData.map(dialog => <FriendsList key={dialog.id} link={dialog.id} name={dialog.name}/>)}
         </ul>
         <div className={s.messages}>
            {getState.messageData.map(message => <Message key={message.id} message={message.text}/>)}
            <DialogsFormRedux onSubmit={onSubmit}/>
         </div>
      </div>
   );
};

export default Dialogs;