import {sendMessageCreator} from "../../redux/dialogsReducer";
import Dialogs from "./Dialogs";
import {connect} from "react-redux";
import {WithAuthRedirect} from "../../hoc/WithAuthRedirect";
import {compose} from "redux";


let mapStateToProps = (state) => {
   return {
      dialogsPage: state.dialogsPage,
   }
};

let mapDispatchToProps = (dispatch) => {
   return {
      // updateNewMessageBody: (body) => {
      //    dispatch(updateNewMessageBodyCreator(body))
      // },
      sendMessageClick: (message) => {
         dispatch(sendMessageCreator(message))
      }
   }
};

export default compose(
   connect(mapStateToProps, mapDispatchToProps),
   WithAuthRedirect
)(Dialogs);