import React from 'react';
import s from "./FriendsList.module.css";
import {NavLink} from "react-router-dom";

const FriendsList = (props) => {
   return (
      <li className={s.friendsList__elem}>
         <NavLink to={"/dialogs/" + props.link} className={s.friendsList__link}
                  activeClassName={s.active}>
            {props.name}
         </NavLink>
      </li>
   );
};

export default FriendsList;