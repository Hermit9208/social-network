import React from 'react';
import classes from './Navbar.module.css';
import NavbarLink from "./NavbarLink/NavbarLink";
import userAvatar from '../../assets/images/user-avatar.png'

const Navbar = (props) => {
   return (
      <nav className={classes.nav}>
         <ul>
            {props.navBarLink.map((link, id) => <NavbarLink link={link} key={id}/>)}
         </ul>
         <div className={classes.friends}>
            <p className={classes.friends__title}>Friends</p>
            <div className={classes.flex}>
               {props.posts.map((post, id) => (
                  <div className={classes.friendsBlock} key={id}>
                     <img src={userAvatar} alt="user-avatar"/>
                     <p className={classes.friendsBlock__name}>{post.name}</p>
                  </div>
               ))}
            </div>
         </div>
      </nav>
   );
};


export default Navbar;