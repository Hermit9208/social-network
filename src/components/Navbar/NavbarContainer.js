import {connect} from "react-redux";
import Navbar from "./Navbar";

let mapStateToProps = (state) => {
   return {
      navBarLink: state.navBarLink,
      posts: state.profilePage.posts
   }
};

let mapDispatchToProps = () => {
   return {

   }
};

const NavbarContainer = connect(mapStateToProps, mapDispatchToProps) (Navbar);

export default NavbarContainer;