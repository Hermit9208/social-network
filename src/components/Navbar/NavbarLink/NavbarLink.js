import React from 'react';
import {NavLink} from "react-router-dom";
import classes from "./NavbarLink.module.css";

const NavbarLink = (props) => {
   const link = `/${props.link}`;
   return (
      <li className={classes.nav__li}>
         <NavLink to={link} className={classes.nav__a} activeClassName={classes.active}>{props.link}</NavLink>
      </li>
   );
};

export default NavbarLink;